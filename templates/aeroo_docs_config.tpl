[start]
interface = ${aeroo_docs_config:interface}
port = ${aeroo_docs_config:port}
oo-server = ${aeroo_docs_config:oo-server}
oo-port = ${aeroo_docs_config:oo-port}
log-file = ${aeroo_docs_config:log-file}
pid-file = var/run/aeroo-docs.pid
spool-directory = tmp/aeroo-docs
spool-expire = 1800

[simple-auth]
username = anonymous
password = anonymous

