## Logrotate configuration file fo Odoo package

${buildout:directory}/var/log/*.log {
    rotate 1
    daily
    copytruncate
    sharedscripts
    missingok
    notifempty
}